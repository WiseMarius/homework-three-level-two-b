#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	connect(ui->horizontalSlider,
		SIGNAL(sliderMoved(int)),
		this,
		SLOT(updateBackgroundColor()));

	connect(ui->horizontalSlider_2,
		SIGNAL(sliderMoved(int)),
		this,
		SLOT(updateBackgroundColor()));

	connect(ui->horizontalSlider_3,
		SIGNAL(sliderMoved(int)),
		this,
		SLOT(updateBackgroundColor()));

	updateBackgroundColor();
}

void MainWindow::updateBackgroundColor() 
{
	QPalette pal = ui->centralWidget->palette();
	pal.setColor(QPalette::Window, QColor(ui->horizontalSlider->value(), ui->horizontalSlider_2->value(), ui->horizontalSlider_3->value()));
	ui->centralWidget->setPalette(pal);
}

MainWindow::~MainWindow()
{
    delete ui;
}
